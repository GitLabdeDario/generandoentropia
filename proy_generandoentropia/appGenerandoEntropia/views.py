PRODUCCION = False

from django.http import HttpResponseRedirect
from django.shortcuts import render
from django.urls import reverse
from django.db import IntegrityError
from django.contrib.auth import authenticate, login, logout
from .models import User

LED = 22
if PRODUCCION:
    import RPi.GPIO as GPIO
    GPIO.setmode(GPIO.BOARD)
    GPIO.setwarnings(False)
    GPIO.setup(LED, GPIO.OUT)
    GPIO.output(LED, 0)

# Create your views here.
def index(request):
    return render(request, "appGenerandoEntropia/index.html")

def losMalosTenianRazon(request):
    return render(request, "appGenerandoEntropia/losMalosTenianRazon.html")

def miscosas(request):
    return render(request, "appGenerandoEntropia/miscosas.html")

def estroncio(request):
    return render(request, "appGenerandoEntropia/estroncio.html")

def reactorArk(request):
    return render(request, "appGenerandoEntropia/reactorArk.html")

def eNanoPiano(request):
    return render(request, "appGenerandoEntropia/eNanoPiano.html")

def numeros_IA(request):
    return render(request, "appGenerandoEntropia/numeros_IA.html")

def loquenosepuededecir(request):
    #return render(request, "appGenerandoEntropia/loquenosepuededecir.html")
    if request.method == "POST":
        #Lee lo que se haya escrito en los campos del form
        username = request.POST["username"]
        password = request.POST["password"]

        #Verifica si usuario y contraseña son correctos (los ingresados en el form)
        user = authenticate(request, username=username, password=password)

        #Si se devuelve un objeto USER, se puede loguear y se rutea a la pagina loquenosepuededecirLogueado
        if user:
            login(request, user)
            return HttpResponseRedirect(reverse("loquenosepuededecirLogueado"))
        #Si el usuario y/o contraseña no son correctos, nos redirige a la página para loguearnos, con un mensaje del error
        else:
            return render(request, "appGenerandoEntropia/loquenosepuededecir.html", {
                "message": "usuario y/o contraseña inválido"
            })

    return render(request, "appGenerandoEntropia/loquenosepuededecir.html")    

def loquenosepuededecirLogueado(request):
    #return render(request, "appGenerandoEntropia/loquenosepuededecirLogueado.html")
    # If no user is signed in, return to login page:     
    if not request.user.is_authenticated:
        return HttpResponseRedirect(reverse("loquenosepuededecir"))
    return render(request, "appGenerandoEntropia/loquenosepuededecirLogueado.html")
    
def led1on(request):
    if PRODUCCION:
        GPIO.output(LED,1)
    return render(request, "appGenerandoEntropia/loquenosepuededecirLogueado.html")
def led1off(request):
    if PRODUCCION:
        GPIO.output(LED,0)
    return render(request, "appGenerandoEntropia/loquenosepuededecirLogueado.html")

def logout_view(request):
    logout(request)
    return render(request, "appGenerandoEntropia/logout.html")

def aikalaperra(request):
    return render(request, "appGenerandoEntropia/aikalaperra.html")

def mazorcaracer(request):
    return render(request, "appGenerandoEntropia/mazorcaracer.html")

def register(request):
    if request.method == "POST":
        username = request.POST["username"]
        email = request.POST["email"]      

        # Ensure password matches confirmation
        password = request.POST["password"]
        confirmation = request.POST["confirmation"]
        if password != confirmation:
            return render(request, "appGenerandoEntropia/register.html", {
                "message": "Las contraseñas no son iguales."
            })

        # Attempt to create new user
        try:
            user = User.objects.create_user(username, email, password)
            user.save()
        except IntegrityError:
            return render(request, "appGenerandoEntropia/register.html", {
                "message": "El usuario ya existe."
            })
        login(request, user)
        return HttpResponseRedirect(reverse("loquenosepuededecirLogueado"))
    else:
        return render(request, "appGenerandoEntropia/register.html")